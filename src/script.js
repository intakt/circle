// object for values with sliders
var parameters = {
  radius : 121,
  // rotations / second
  rps : 0.14142,
  // phase or time
  phase : 0 ,
  center : {
    x : window.innerWidth * 0.5,
    y : window.innerHeight * 0.5,
  },
  show : "xy"
}

var gui = new dat.GUI()

// time of the last frame for calculating deltatime in draw function
var lastframe = 0

function setup(){
  createCanvas(window.innerWidth,window.innerHeight)
  // add gui sliders
  gui.add(parameters, "radius", 0, Math.min(window.innerWidth, window.innerHeight) * 0.5)
  gui.add(parameters, "phase", 0.0, 1.0).name("phase [0..1]").step(0.01).listen()
  gui.add(parameters, "rps", 0, 20).name("rotations per second").step(0.1)
  gui.add(parameters.center, "x", 0, window.innerWidth).name("center.x").listen().step(0.1)
  gui.add(parameters.center, "y", 0, window.innerHeight).name("center.y").listen().step(0.1)
  gui.add(parameters, "show", ["xy", "x", "y", "none"])
}

function draw(){
  background(120)

  // deltatime calculations

  let now = millis() // get milliseconds passed since the site is loaded
  // calculate time since last frame for framerate independent timing
  let deltatime =  now - lastframe
  
  lastframe = now
  
  

  // THE POINT /////////////////////////////////////////////////////

  // phase
  
  // increment phase with deltatime
  let new_phase = parameters.phase + parameters.rps * deltatime * 0.001
  // keep phase between 0 and 1
  parameters.phase = new_phase - Math.floor(new_phase)


  // angles

  // calculate sine, cosine at phase (results are in the range of -1 to 1)
  let sine = Math.sin(parameters.phase * 2 * Math.PI)
  let cosine = Math.cos(parameters.phase * 2 * Math.PI)


  // resulting position

  // multiply results with radius to get coordinates circling around (x : 0, y : 0)
  let x = parameters.radius * cosine
  let y = parameters.radius * sine
  // offset coordinates to center position
  let offset_pos = {
    x : parameters.center.x + x, 
    y : parameters.center.y + y
  }
  // override with just center if only a single axis is selected
  if(parameters.show == "y") offset_pos.x = parameters.center.x
  else if(parameters.show == "x") offset_pos.y = parameters.center.y
  
  // draw yellow circle at pos
  fill(255, 200, 0)
  ellipse(offset_pos.x, offset_pos.y, 10)
  //////////////////////////////////////////////////////////////




  // INFORMATION PLOTTING

  if(parameters.show != "none"){ 
    //show radius
    let radiustext = {
      x : parameters.center.x + cosine * parameters.radius * 0.5,
      y : parameters.center.y + sine * parameters.radius * 0.5
    }
    if(parameters.show == "y") radiustext.x = parameters.center.x
    else if(parameters.show == "x") radiustext.y = parameters.center.y
    stroke(100, 0, 242)
    line(parameters.center.x, parameters.center.y, offset_pos.x, offset_pos.y)
    fill(100, 0, 242)
    noStroke()
    text("radius : " + parameters.radius, radiustext.x, radiustext.y + 10)
    
    // show center and pos coordinates
    noStroke()
    fill("#f00")
    text("x : " + offset_pos.x.toFixed(2), offset_pos.x+10,offset_pos.y - 2)
    fill("#0fd")
    text("y : " + offset_pos.y.toFixed(2), offset_pos.x+4, offset_pos.y + 16)
    noFill()
    stroke(80)
    ellipse(parameters.center.x, parameters.center.y, 10)
    fill(80)
    noStroke()
    text("center", parameters.center.x, parameters.center.y - 15 )
    text("x : " + parameters.center.x.toFixed(2), parameters.center.x + 10, parameters.center.y)
    text("y : " + parameters.center.y.toFixed(2), parameters.center.x + 10, parameters.center.y + 15)
    noFill()

    // show circle path
    stroke(100)  
    ellipse(parameters.center.x, parameters.center.y, parameters.radius * 2)
    

    if(parameters.show == "x" || parameters.show == "xy"){
      noFill()
      stroke(100)

      // plot cosine along a full cycle 
      for(var cs = 0; cs < 100; cs ++){
        let c = 
          parameters.radius 
          * Math.cos((parameters.phase) * 2 * Math.PI + 2 * Math.PI * (cs / 100))
          
        ellipse(parameters.center.x + c, (cs/100) * window.innerHeight, 10)
      }
      // show current with red
      stroke("#f00")
      line(offset_pos.x, offset_pos.y, offset_pos.x, window.innerHeight)
      ellipse(offset_pos.x, window.innerHeight, 10)

      // show cos equation and current value
      noStroke() 
      fill("#f00")
      text("cosine = Math.cos( phase * 2\u03c0 ) = " + cosine.toFixed(2), offset_pos.x + 10, window.innerHeight - 30)
      text("x = center.x + radius * cosine", offset_pos.x + 10, window.innerHeight - 10)
    }

    // same as the above but with sine
    if(parameters.show == "y" || parameters.show == "xy"){
      noFill()
      stroke(100)

      for(var sn = 0; sn < 100; sn ++){
        let s = 
          parameters.radius 
          * Math.sin((parameters.phase) * 2 * Math.PI + 2 * Math.PI * (sn / 100))

        ellipse((sn/100) * window.innerWidth, parameters.center.y + s, 10)
      }

      stroke("#0fd")
      line(0, offset_pos.y, offset_pos.x, offset_pos.y)
      ellipse(0, offset_pos.y, 10)
      
      noStroke()
      fill("#0fd")
      text("sine = Math.sin( phase * 2\u03c0 ) = " + sine.toFixed(2), 10, offset_pos.y - 30)
      text("y = center.y + radius * sine", 10, offset_pos.y - 10)
    }
  }
}

function windowResized(){
  window.location.reload()
}
